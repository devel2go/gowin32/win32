/* frame.go */

package win32

import (
	//"log"
	//"syscall"
	//"unsafe"
	"gitlab.com/devel2go/gowin32/win32/w32"
)

type FrameBorderStyle int

const (
	FrameBorderNone FrameBorderStyle = iota
	FrameBorderSingleLine
	FrameBorderSunken
	FrameBorderSunkenThick
	FrameBorderRaised
)

func borderStyleEx(b FrameBorderStyle) uint {
	if b == FrameBorderSunken {
		return w32.WS_EX_STATICEDGE
	}
	if b == FrameBorderSunkenThick {
		return w32.WS_EX_CLIENTEDGE
	}
	return 0
}

func borderStyle(b FrameBorderStyle) uint {
	if b == FrameBorderSingleLine {
		return w32.WS_BORDER
	}
	if b == FrameBorderRaised {
		return w32.WS_DLGFRAME
	}
	return 0
}

type GoFrameControl struct {
	goWidget
	goObject
	border FrameBorderStyle
}

func (f *GoFrameControl) destroy() {
	for id, _ := range f.controls {
		f.controls[id] = nil	// delete control structure
		delete(f.controls, id)
	}
}

func (f *GoFrameControl) wid() (*goWidget) {
	return &f.goWidget
}

func (f *GoFrameControl) SetBorderStyle(s FrameBorderStyle) {
	f.border = s
	if f.hWnd != 0 {
		style := uint(w32.GetWindowLongPtr(f.hWnd, w32.GWL_STYLE))
		style = style &^ w32.WS_BORDER &^ w32.WS_DLGFRAME
		style |= borderStyle(s)
		w32.SetWindowLongPtr(f.hWnd, w32.GWL_STYLE, uintptr(style))

		exStyle := uint(w32.GetWindowLongPtr(f.hWnd, w32.GWL_EXSTYLE))
		exStyle = exStyle &^ w32.WS_EX_STATICEDGE &^ w32.WS_EX_CLIENTEDGE
		exStyle |= borderStyleEx(s)
		w32.SetWindowLongPtr(f.hWnd, w32.GWL_EXSTYLE, uintptr(exStyle))
		w32.InvalidateRect(f.wid().parent.hWnd, nil, true)
	}
}