/* object.go */

package win32

import (
	"log"
)



type goObject struct {
	parent GoObject
	controls  map[int]GoObject
}


func (ob *goObject) handleNotification(cmd uintptr) {
	log.Println("GoObject::handleNotification()")
}

func (ob *goObject) addControl(id int, control GoObject) {
	ob.controls[id] = control
}

func (ob *goObject) control(id int) (GoObject) {
	return ob.controls[id]
}

/*func (ob *goObject) mainWindow() (GoObject) {
	obj := *ob
	for {
		if obj.parent == nil {
			break
		}
		obj = obj.parent
	}
	return obj	
}*/

func (ob *goObject) parentControl() (GoObject) {
	return ob.parent
}

func (ob *goObject) removeControl(id int) {
	//delete ob.controls[id].delete
	delete(ob.controls, id)
}

func (ob *goObject) onWM_COMMAND(w, l uintptr) {
	ob.parentControl().onWM_COMMAND(w, l)
}

func (ob *goObject) onWM_DRAWITEM(w, l uintptr) {
	//ob.parentControl().onWM_DRAWITEM(w, l)
}

func (ob *goObject) onWM_NOTIFY(w, l uintptr) {
	//ob.parentControl().onWM_NOTIFY(w, l)
}
	
