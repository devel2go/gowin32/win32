package win32

import (
	"errors"
	"unicode/utf16"

	"gitlab.com/devel2go/gowin32/win32/w32"
)

func GoFont(desc GoFontDesc) (*goFont, error) {
	var weight int32 = w32.FW_NORMAL
	if desc.Bold {
		weight = w32.FW_BOLD
	}
	byteBool := func(b bool) byte {
		if b {
			return 1
		}
		return 0
	}
	logfont := w32.LOGFONT{
		Height:         int32(desc.Height),
		Width:          0,
		Escapement:     0,
		Orientation:    0,
		Weight:         weight,
		Italic:         byteBool(desc.Italic),
		Underline:      byteBool(desc.Underlined),
		StrikeOut:      byteBool(desc.StrikedOut),
		CharSet:        w32.DEFAULT_CHARSET,
		OutPrecision:   w32.OUT_CHARACTER_PRECIS,
		ClipPrecision:  w32.CLIP_CHARACTER_PRECIS,
		Quality:        w32.DEFAULT_QUALITY,
		PitchAndFamily: w32.DEFAULT_PITCH | w32.FF_DONTCARE,
	}
	copy(logfont.FaceName[:], utf16.Encode([]rune(desc.Name)))
	handle := w32.CreateFontIndirect(&logfont)
	if handle == 0 {
		return nil, errors.New("wui.NewFont: unable to create font, please check your description")
	}
	return &goFont{Desc: desc, handle: handle}, nil
}

type GoFontDesc struct {
	Name       string
	Height     int
	Bold       bool
	Italic     bool
	Underlined bool
	StrikedOut bool
}

type goFont struct {
	Desc   GoFontDesc
	handle w32.HFONT
}