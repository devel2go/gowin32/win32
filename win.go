/* win.go */

package win32

import (
	"log"
	"syscall"
	"gitlab.com/devel2go/gowin32/win32/w32"
)

var GoTestView *GoWindowView

var nextCtrlId int = 100
var nextWinId int = -1

func nextWindowId() (int) {
	nextWinId++
	return nextWinId
}

func nextControlId() (int) {
	nextCtrlId++
	return nextCtrlId
}

type GoObject interface {
	addControl(id int, control GoObject)
	control(id int) (GoObject)
	handleNotification(cmd uintptr)
	//mainWindow() (GoObject)
	parentControl() (GoObject)
	removeControl(int)
	
	onWM_COMMAND(w, l uintptr)
	onWM_DRAWITEM(w, l uintptr)
	onWM_NOTIFY(w, l uintptr)
	destroy()
	wid() (*goWidget)
}

func GoWindow(parent GoObject) (hWin *GoWindowView) {
	var p_widget *goWidget = nil
	var p_hWnd w32.HWND = 0

	a := GoApp()
	if a == nil {
		log.Fatal("ERROR creating GoMainWindow: Create GoApplication First")
	}

	// Top window properties 
	p_instance := a.hInstance
	p_nextid := nextWindowId()
	
	// Owned window properties inherited from parent window
	if parent != nil {
		p_widget = parent.wid()
		p_hWnd = p_widget.hWnd
		p_instance = p_widget.instance
	}
	

	widget := goWidget{
		className:  	goApp.appName,
		windowName:		"",
		style:     		w32.WS_OVERLAPPEDWINDOW,
		exStyle:		0,
		state:      	w32.SW_HIDE,
		x:          	0,
		y:          	0,
		width:      	600,
		height:     	400,
		hWndParent: 	p_hWnd,
		id: 			p_nextid,
		instance: 		p_instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		font:        	nil,
		widgets:		map[int]*goWidget{},
		onShow:        	nil,
		onClose:       	nil,
		onCanClose:    	nil,
		onMouseMove:   	nil,
		onMouseWheel:  	nil,
		onMouseDown:   	nil,
		onMouseUp:     	nil,
		onKeyDown:     	nil,
		onKeyUp:       	nil,
		onResize:      	nil,
	}
	object := goObject{nil, map[int]GoObject{}}
	hWin = &GoWindowView{widget, object, "", 0}	
	_, err := hWin.Create()
	if err != nil {
		log.Println("Problem creating GoMainWindow :", err)
	} else {
		a.AddWindow(hWin, p_nextid)
	}
	//registerMainWindow(hWin)
	return hWin
}

func GoDialogWindow(parent GoObject) (hWin *GoWindowView) {
	var p_widget *goWidget = nil
	var p_hWnd w32.HWND = 0

	a := GoApp()
	if a == nil {
		log.Fatal("ERROR creating GoMainWindow: Create GoApplication First")
	}

	// Top window properties 
	p_instance := a.hInstance
	p_nextid := nextWindowId()
	
	// Owned window properties inherited from parent window
	if parent != nil {
		p_widget = parent.wid()
		p_hWnd = p_widget.hWnd
		p_instance = p_widget.instance
	}

	widget := goWidget{
		className:  	goApp.appName,
		windowName:		"",
		style:     		w32.WS_OVERLAPPED | w32.WS_CAPTION | w32.WS_SYSMENU,
		exStyle:		0,
		state:      	w32.SW_HIDE,
		x:          	0,
		y:          	0,
		width:      	400,
		height:     	300,
		hWndParent: 	p_hWnd,
		id: 			p_nextid,
		instance: 		p_instance,
		param:			0,
		hWnd: 			0,
		parent:  		p_widget,
		disabled: 		false,
		visible:   		false,
		font:        	nil,
		widgets:		map[int]*goWidget{},
		onShow:        	nil,
		onClose:       	nil,
		onCanClose:    	nil,
		onMouseMove:   	nil,
		onMouseWheel:  	nil,
		onMouseDown:   	nil,
		onMouseUp:     	nil,
		onKeyDown:     	nil,
		onKeyUp:       	nil,
		onResize:      	nil,
	}
	object := goObject{nil, map[int]GoObject{}}
	hWin = &GoWindowView{widget, object, "", 0}	
	_, err := hWin.Create()
	if err != nil {
		log.Println("Problem creating GoMainWindow :", err)
	} else {
		a.AddWindow(hWin, p_nextid)
	}
	//registerMainWindow(hWin)
	return hWin		
}

func GoButton(parent GoObject) (hButton *GoButtonControl) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}

	p_nextid := nextControlId()
	p_widget := parent.wid()

	widget := goWidget{"BUTTON", "Button", w32.WS_CHILD | w32.BS_PUSHBUTTON, 0, w32.SW_HIDE, 10, 50, 100, 40, p_widget.hWnd, p_nextid, p_widget.instance, 0, 0, p_widget, false, false, nil, map[int]*goWidget{}, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil}
	object := goObject{parent, map[int]GoObject{}}
	b := &GoButtonControl{widget, object, nil, nil, nil, nil, nil, nil}
	
	_, err := b.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(p_nextid, b)
		GoApp().AddControl(b, p_nextid)
	}
	if sizeToView == true {
		GoTestView.SizeToControl(b)
	}
	return b
}

func GoFrame(parent GoObject) (hButton *GoFrameControl) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}

	p_nextid := nextControlId()
	p_widget := parent.wid()


	widget := goWidget{"STATIC", "", w32.WS_THICKFRAME, 0, w32.SW_HIDE, 10, 100, 200, 140, p_widget.hWnd, p_nextid, p_widget.instance, 0, 0, p_widget, false, false, nil, map[int]*goWidget{}, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil}
	object := goObject{parent, map[int]GoObject{}}
	f := &GoFrameControl{widget, object, FrameBorderSingleLine}
	
	_, err := f.Create()

	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(p_nextid, f)
		GoApp().AddControl(f, p_nextid)
	}

	w32.SetWindowSubclass(f.hWnd, syscall.NewCallback(func(
		window w32.HWND,
		msg uint32,
		wParam, lParam uintptr,
		subclassID uintptr,
		refData uintptr,
	) uintptr {
		switch msg {
		case w32.WM_COMMAND:
			f.onWM_COMMAND(wParam, lParam)
			return 0
		case w32.WM_DRAWITEM:
			f.onWM_DRAWITEM(wParam, lParam)
			return 0
		case w32.WM_NOTIFY:
			f.onWM_NOTIFY(wParam, lParam)
			return 0
		default:
			return w32.DefSubclassProc(window, msg, wParam, lParam)
		}
	}), 0, 0)
	f.SetBorderStyle(FrameBorderSingleLine)
	if sizeToView == true {
		GoTestView.SizeToControl(f)
	}
	return f
}

func GoLabel(parent GoObject) (hLabel *GoLabelControl) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}

	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{"STATIC", "Label", w32.WS_CHILD | w32.SS_CENTERIMAGE | w32.WS_THICKFRAME | w32.SS_LEFT, 0, w32.SW_HIDE, 10, 10, 100, 40, p_widget.hWnd, p_nextid, p_widget.instance, 0, 0, p_widget, false, false, nil, map[int]*goWidget{}, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil}
	object := goObject{parent, map[int]GoObject{}}
	l := &GoLabelControl{widget, object, "", w32.SS_LEFT}
	
	_, err := l.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(p_nextid, l)
		GoApp().AddControl(l, p_nextid)
		
	}
	if sizeToView == true {
		GoTestView.SizeToControl(l)
	}
	return l
}

func GoLabelEx(parent GoObject, text string) (hLabel *GoLabelControl) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}

	
	p_nextid := nextControlId()
	p_widget := parent.wid()
	widget := goWidget{"STATIC", text, w32.WS_CHILD | w32.SS_CENTERIMAGE | w32.WS_THICKFRAME | w32.SS_LEFT, 0, w32.SW_HIDE, 10, 10, 100, 40, p_widget.hWnd, p_nextid, p_widget.instance, 0, 0, p_widget, false, false, nil, map[int]*goWidget{}, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil}
	log.Println("p_nextid = ", p_nextid)
	object := goObject{parent, map[int]GoObject{}}
	l := &GoLabelControl{widget, object, text, w32.SS_LEFT}
	
	_, err := l.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(p_nextid, l)
		GoApp().AddControl(l, p_nextid)
	}
	if sizeToView == true {
		GoTestView.SizeToControl(l)
	}
	return l
}

func GoCheckBox(parent GoObject, text string) (hCheckBox *GoCheckBoxControl) {
	var sizeToView bool
	
	// if control has no parent create GoDialogWindow and make it parent
	if parent == nil {
		parent = StockDialogWindow()
		log.Println("No Parent")
		sizeToView = true
	}

	p_nextid := nextControlId()
	p_widget := parent.wid()

	//c.textControl.create(id, 0, "BUTTON", w32.WS_TABSTOP|w32.BS_AUTOCHECKBOX)
	//w32.SendMessage(c.handle, w32.BM_SETCHECK, toCheckState(c.checked), 0)
									   
	widget := goWidget{"BUTTON", text, w32.BS_AUTOCHECKBOX, 0, w32.SW_HIDE, 10, 50, 100, 40, p_widget.hWnd, p_nextid, p_widget.instance, 0, 0, p_widget, false, false, nil, map[int]*goWidget{}, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil}
	object := goObject{parent, map[int]GoObject{}}
	c := &GoCheckBoxControl{widget, object, false, nil}
	
	_, err := c.Create()
	if err != nil {
		log.Println("ERROR. :", err)
	} else {
		log.Println("parent.addControl :", p_nextid)
		parent.addControl(p_nextid, c)
		GoApp().AddControl(c, p_nextid)
	}
	if sizeToView == true {
		GoTestView.SizeToControl(c)
	}
	return c
}

func GoDeskTop() (hdesktop *GoDeskTopWindow) {

	ret := w32.GetDesktopWindow()
	hWnd := w32.HWND(ret)
	screen := goWidget{"", "", 0, 0, 0, w32.CW_USEDEFAULT, w32.CW_USEDEFAULT, w32.CW_USEDEFAULT, w32.CW_USEDEFAULT, 0, 0, 0, 0, 0, nil, false, false, nil, map[int]*goWidget{}, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil}
	object := goObject{nil, map[int]GoObject{}}
	hdesktop = &GoDeskTopWindow{screen, object}
	hdesktop.hWnd = hWnd
	hdesktop.instance = goApp.hInstance
	return hdesktop
}

func StockDialogWindow() (hWin *GoWindowView) {
	//var GoMainView *GoWindowView
	GoTestView = GoDialogWindow(nil)
	GoTestView.Show()
	return GoTestView
}