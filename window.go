/* window.go */

package win32

import (
	"log"
	"gitlab.com/devel2go/gowin32/win32/w32"
)

const IDM_DOIT uint32 = 1
const IDM_QUIT uint32 = 2

type GoWindowView struct {
	goWidget		// embedded widget
	goObject 		// embedded object
	
	title         	string
	//background    w32.HBRUSH
	
	//icon          uintptr
	//menu          *Menu
	//menuStrings   []*MenuString
	lastFocus		w32.HWND
}

// Overide goWidget.Create() function for window
func (w *GoWindowView) Create() (*goWidget, error) {
	var hWnd w32.HWND
	var err error
	
	hWnd, err = w32.CreateWindowExStr(
		w.goWidget.exStyle,
		w.goWidget.className,
		w.goWidget.windowName,
		w.goWidget.style,
		w.goWidget.x, w.goWidget.y, w.goWidget.width, w.goWidget.height,
		w.goWidget.hWndParent,
		w32.HMENU(0),
		w.goWidget.instance,
		nil,
	)
		//w.className, w.windowName, w.style, w.x, w.y, w.width, w.height, w.hWndParent, w32.HMENU(w.id), w.instance)
	
	if err != nil {
		log.Println("goWidget id = ", w.goWidget.id)
		log.Println("Failed to create widget.")
	} else {
		w.goWidget.hWnd = hWnd
		// Top Level Window does not have parent
		if w.goWidget.parent != nil {
			w.goWidget.parent.widgets[w.goWidget.id] = &(w.goWidget)
		}
	}

	return&(w.goWidget), err
}

// type RECT struct {Left, Top, Right, Bottom int32}

func (w *GoWindowView) SizeToControl(control GoObject) {

	widget := control.wid()
	if w == nil {
		log.Println("Window does not exist!")
	}
	log.Println("WindowView", w)
	// RECT structures
	clirect := w32.GetClientRect(w.hWnd)
	log.Println("Window ClientRect - left:", clirect.Left, "top:", clirect.Top, "right:", clirect.Right, "bottom:", clirect.Bottom)
	winrect := w32.GetWindowRect(w.hWnd)
	ctlrect := w32.GetWindowRect(widget.hWnd)
	winFrameWidth := int(winrect.Right - winrect.Left)
	winFrameHeight := int(winrect.Bottom - winrect.Top)
log.Println("Window Frame - width:", winFrameWidth, "height:", winFrameHeight)
	cliFrameWidth := int(clirect.Right - clirect.Left)
	cliFrameHeight := int(clirect.Bottom - clirect.Top)
log.Println("Window Client - width:", cliFrameWidth, "height:", cliFrameHeight)
	ctlFrameWidth := int(ctlrect.Right - ctlrect.Left)
	ctlFrameHeight := int(ctlrect.Bottom - ctlrect.Top)
log.Println("Control Frame - width:", ctlFrameWidth, "height:", ctlFrameHeight)
	//ctlFrameWidth := widget.width
	//ctlFrameHeight := widget.height
	w.x = int(winrect.Left)
	w.y = int(winrect.Top)
	w.width = winFrameWidth - cliFrameWidth + ctlFrameWidth
	w.height = winFrameHeight - cliFrameHeight + ctlFrameHeight + 20  // Margin 20
	log.Println("Window - x:", w.x, "y:", w.y, "width:", w.width, "height:", w.height)
	w32.MoveWindow(
		w.hWnd,
		w.x, w.y, w.width, w.height,
		true,
	)
	/*w32.SetWindowPos(
			w.hWnd, 0,
			w.x, w.y, w.width, w.height,
			w32.SWP_NOOWNERZORDER|w32.SWP_NOZORDER,
	)*/
}

func (w *GoWindowView) SetTitle(title string) {
	w.title = title
	if w.hWnd != 0 {
		w32.SetWindowText(w.hWnd, title)
	}
}

func (w *GoWindowView) TextOut(text string) {
	hDC := w32.GetDC(w.hWnd)
	w32.TextOut(hDC, 10, 20, text)
	w32.ReleaseDC(w.hWnd, hDC)
}

func (w *GoWindowView) Title() string {
	return w.title
}

/*func (mw *GoMainWindowView) Update() {
	w32.UpdateWindow(mw.hWnd)
}*/

/*func (w *GoWindowView) WndProc(hWnd w32.HWND, msg uint32, wParam uintptr, lParam uintptr) uintptr {
	mouseX := int(lParam & 0xFFFF)
	mouseY := int(lParam&0xFFFF0000) >> 16
	switch msg {
		case w32.WM_ACTIVATE:
			active := wParam != 0
			if active {
				if w.lastFocus != 0 {
					w32.SetFocus(w.lastFocus)
				}
			} else {
				w.lastFocus = w32.GetFocus()
			}
			return 0
		case w32.WM_CLOSE:
			if w.onCanClose != nil {
				if w.onCanClose() == false {
					return 0
				}
			}
			if w.Parent() != nil {
				w32.EnableWindow(w.hWndParent, true)
				w32.SetForegroundWindow(w.hWndParent)
			}
			if w.onClose != nil {
				w.onClose()
			}
			return 0
		
		case w32.WM_COMMAND:
			w.onWM_COMMAND(wParam, lParam)
		case w32.WM_DESTROY:
			w32.PostQuitMessage(0)
			return 0	
			//switch uint32(wparam) {
			//	case IDM_DOIT:
			//		hDC = w32.GetDC(hWnd)
			//		w32.TextOut(hDC, 10, 20, "Ok, I did it!")
			//		w32.ReleaseDC(hWnd, hDC)
			//	case IDM_QUIT:
			//		w32.DestroyWindow(hWnd)
			//}
		case w32.WM_KEYDOWN:
			if w.onKeyDown != nil {
				w.onKeyDown(int(wParam))
				return 0
			}
		case w32.WM_KEYUP:
			if w.onKeyUp != nil {
				w.onKeyUp(int(wParam))
				return 0
			}
		case w32.WM_LBUTTONDOWN, w32.WM_MBUTTONDOWN, w32.WM_RBUTTONDOWN:
			if w.onMouseDown != nil {
				b := MouseButtonLeft
				if msg == w32.WM_MBUTTONDOWN {
					b = MouseButtonMiddle
				}
				if msg == w32.WM_RBUTTONDOWN {
					b = MouseButtonRight
				}
				w.onMouseDown(b, mouseX, mouseY)
			}
			return 0
		case w32.WM_LBUTTONUP, w32.WM_MBUTTONUP, w32.WM_RBUTTONUP:
			if w.onMouseUp != nil {
				b := MouseButtonLeft
				if msg == w32.WM_MBUTTONUP {
					b = MouseButtonMiddle
				}
				if msg == w32.WM_RBUTTONUP {
					b = MouseButtonRight
				}
				w.onMouseUp(b, mouseX, mouseY)
			}
			return 0
		case w32.WM_MOUSEMOVE:
			if w.onMouseMove != nil {
				w.onMouseMove(mouseX, mouseY)
				return 0
			}
		}
		ret := w32.DefWindowProc(hWnd, msg, wParam, lParam)
		return ret
	}

	func (w *GoWindowView) handleNotification(cmd uintptr) {
	log.Println("GoMainWindow::handleNotification..............")
}*/

func (w *GoWindowView) onWM_COMMAND( wParam uintptr, lParam uintptr) {
	log.Println("onWM_COMMAND..............")
	log.Println("wParam....ControlID....", wParam)
	wParamH := (wParam & 0xFFFF0000) >> 16
	wParamL := wParam & 0xFFFF
	lParamH := (lParam & 0xFFFFFFFF00000000) >> 32
	lParamL := lParam & 0xFFFFFFFF
	log.Println("wParamH....", wParamH)
	log.Println("wParamL....ControlID....", wParamL)
	log.Println("lParamH....Notification....", lParamH)
	log.Println("lParamL....WindowHandle....", lParamL)
	if lParam != 0 {
		// control clicked
		index := wParamL	// controlID
		cmd := lParamH		// BN_CLICKED
		log.Println("cmd....", cmd)
		if cmd == w32.BN_CLICKED {
			log.Println("w32.BN_CLICKED..............")

			// get widget from GoApp.winStack using controlID and trigger function widget.onClick
			// which in turn triggers function button.onClick()
			goApp.Controls(index).handleNotification(cmd)
		}
		/*if index < uintptr(len(w.controls)) {
			w.controls[index].handleNotification(cmd)
		}*/
	} else {
		if wParamH == 0 {
			// low word of w contains menu ID
			id := uint32(wParamL)
			switch id {
				case IDM_DOIT:
					/*hDC := w32.GetDC(mw.hWnd)
					w32.TextOut(hDC, 10, 20, "Ok, I did it!")
					w32.ReleaseDC(mw.hWnd, hDC)*/
				case IDM_QUIT:
					w32.DestroyWindow(w.hWnd)
			}
			/*if 0 <= id && id < len(w.menuStrings) {
				f := w.menuStrings[id].onClick
				if f != nil {
					f()
				}
			}*/ 
		} else if wParamH == 1 {
				// low word of w contains accelerator ID
			//index := int(wParamL)
			/*if f := w.shortcuts[index].f; f != nil {
				f()
			}*/
		}

	}
}

func (w *GoWindowView) onWM_DRAWITEM(wParam uintptr, lParam uintptr) {
}

func (w *GoWindowView) onWM_NOTIFY(wParam uintptr, lParam uintptr) {
}

func (w *GoWindowView) destroy() () {
	for id, _ := range w.controls {
		w.controls[id] = nil	// delete control structure
		delete(w.controls, id)	// remove from Control Stack
	}
}

func (w *GoWindowView) wid() (*goWidget) {
	return &w.goWidget
}